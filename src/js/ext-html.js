import Print from './print'
import { cleanUp, addWrapper } from './functions'

export default {
  print: (params, printFrame) => {
    // Format pdf url
    params.printable = /^(blob|http)/i.test(params.printable)
      ? params.printable
      : window.location.origin + (params.printable.charAt(0) !== '/' ? '/' + params.printable : params.printable)

    // Get the file through a http request (Preload)
    let req = new window.XMLHttpRequest()
    req.responseType = 'html'

    // note: here example of set request header
    // req.setRequestHeader

    req.addEventListener('load', () => {
      // Check for errors
      if ([200, 201].indexOf(req.status) === -1) {
        cleanUp(params)
        params.onError(req.statusText, req)

        // Since we don't have a html document available, we will stop the print job
        return
      }

      // @review: is really `addWrapper` needed here
      params.htmlData = addWrapper(req.response, params)
      // params.htmlData = req.response

      //   Print html element contents
      send(params, printFrame)
    })

    req.onreadystatechange = _ => {
      if (req.readyState === 4) {
        const allResponseHeaders = (req.getAllResponseHeaders() || '').split('\r\n')
        const headers = allResponseHeaders.reduce((acc, current, i) => {
          let [ key, value ] = current.split(': ')
          if (key.length) acc[key] = value
          return acc
        }, {})
        params.onSuccess({ headers })
      }
    }

    req.open('GET', params.printable, true)
    req.send()
  }
}

function send (params, printFrame) {
  printFrame.setAttribute('src', params.printable)
  Print.send(params, printFrame)
}
